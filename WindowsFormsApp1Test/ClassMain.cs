﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1Test
{
    public class ClassMain
    {
        //constantes afin de pouvoir reinitialiser les valeurs
        public const int combinaisonConst = 3; //nb minimum de combinaisons de piece a faire pour gagner des points
        public const int colConst = 6; //nb de colonnes
        public const int rowConst = 12; //nb de lignes
        public const int posIntConst = 3;  //position initiale de la piece
        public const bool optionsOuvertesConst = false; //verifie si le menu des options est ouvert
        public const int timerInitConst = 50; //Temps qui passe avant la chute

        //les memes valeurs mais modifiables
        public static int combinaison = combinaisonConst; 
        public static int col = colConst;
        public static int row = rowConst;
        public static int posInt = posIntConst;
        public static bool optionsOuvertes = optionsOuvertesConst;
        public static int timerInit = timerInitConst;
    }
}
