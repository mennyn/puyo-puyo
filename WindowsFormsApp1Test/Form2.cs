﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1Test
{
    public partial class Options : Form
    {
        public Options()
        {
            InitializeComponent();
        }

        //Variable temporaires ayant la valeur des variables globales afin de pouvoir les modifier sans affecter le reste
        int tempCol = ClassMain.col;
        int tempCombi = ClassMain.combinaison;
        int tempDrop = ClassMain.timerInit;
        int tempPos = ClassMain.posInt;
        int tempRow = ClassMain.row;

        private void Options_Load(object sender, EventArgs e)
        {
            BoxCol.Text = ClassMain.col.ToString();
            BoxCombi.Text = ClassMain.combinaison.ToString();
            BoxDrop.Text = ClassMain.timerInit.ToString();
            BoxPos.Text = ClassMain.posInt.ToString();
            BoxRow.Text = ClassMain.row.ToString();
        }

        private void dropVerif(object sender, EventArgs e) //Verifie si ce qui est rentre est correct pour la vitesse de chute
        {
            if (verification(BoxDrop.Text))
            {
                BoxDrop.Text = ClassMain.timerInit.ToString();
            }
            else
            {
                if (Int32.Parse(BoxDrop.Text) > 0)
                {
                    tempDrop = Int32.Parse(BoxDrop.Text);
                    BoxDrop.Text = tempDrop.ToString();
                }
                else
                {
                    MessageBox.Show("Il ne faut pas que le timer \n soit egal a 0");
                    BoxDrop.Text = ClassMain.timerInit.ToString();
                }
            }
        }

        private void colVerif(object sender, EventArgs e)//Verifie si ce qui est rentre est correct pour le nb de colonnes
        {
            if (verification(BoxCol.Text))
            {
                BoxCol.Text = ClassMain.col.ToString();
            }
            else
            {
                if (Int32.Parse(BoxCol.Text) > 2)
                {
                    tempCol = Int32.Parse(BoxCol.Text);
                    BoxCol.Text = tempCol.ToString();
                }
                else
                {
                    MessageBox.Show("nb de colonnes trop petite \n Il faut qu'elles soient superieur a 2");
                    BoxCol.Text = ClassMain.col.ToString();
                }
            }
        }

        private void rowVerif(object sender, EventArgs e) //Verifie si ce qui est rentre est correct pour le nb de lignes
        {
            if (verification(BoxRow.Text))
            {
                BoxRow.Text = ClassMain.row.ToString();
            }
            else
            {
                if (Int32.Parse(BoxRow.Text) > 2)
                {
                    tempRow = Int32.Parse(BoxRow.Text);
                    BoxRow.Text = tempRow.ToString();
                }
                else
                {
                    MessageBox.Show("nb de lignes trop petite \n Il faut qu'elles soient superieur a 2");
                    BoxRow.Text = ClassMain.row.ToString();
                }
            }
        }

        private void posVerif(object sender, EventArgs e) //Verifie si ce qui est rentre est correct pour la position intitiale de la piece
        {
            if (verification(BoxPos.Text))
            {
                BoxPos.Text = ClassMain.posInt.ToString();
            }
            else
            {
                if (Int32.Parse(BoxPos.Text) < tempCol)
                {
                    tempPos = Int32.Parse(BoxPos.Text);
                    BoxPos.Text = tempPos.ToString();
                }
                else
                {
                    MessageBox.Show("Position trop grande \n Elle doit etre comprise entre 0 et " +(tempCol - 1).ToString());
                    BoxPos.Text = ClassMain.posInt.ToString();
                }
            }
        }

        private void comboVerif(object sender, EventArgs e) //Verifie si ce qui est rentré est correct pour le nb de pieces necessaire pour un combo
        {
            if (verification(BoxCombi.Text))
            {
                BoxCombi.Text = ClassMain.combinaison.ToString();
            }
            else
            {
                if (Int32.Parse(BoxCombi.Text) < tempCol && Int32.Parse(BoxCombi.Text) > 1)
                {
                    tempCombi = Int32.Parse(BoxCombi.Text);
                    BoxCombi.Text = tempCombi.ToString();
                }
                else
                {
                    if (tempCol - 1 == 2)
                    {
                        tempCombi = 2;
                        BoxCombi.Text = tempCombi.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Combinaison trop grande \n Il faut qu'elle soit comprise entre 2 et " + (tempCol - 1).ToString());
                        BoxCombi.Text = ClassMain.combinaison.ToString();
                    }
                }
            }
        }

        public bool verification(String text) //verifie si ce qui est mis dans les formulaires est bien un nombre positif
        {
            bool erreur = false;
            int number = 0; ;
            try
            {
                number = Int32.Parse(text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Erreur, ne mettez que des nombres");
                erreur = true;
            }
            catch (OverflowException)
            {
                MessageBox.Show("Erreur, nombre trop grand");
                erreur = true;
            }
            if (number < 0)
            {
                MessageBox.Show("Erreur, nombre negatif");
                erreur = true;
            }
            return erreur;
        }

        private void Reinitialiser_Click(object sender, EventArgs e) //reinitialise les valeurs
        {
            tempCol = ClassMain.colConst;
            tempCombi = ClassMain.combinaisonConst;
            tempDrop = ClassMain.timerInitConst;
            tempPos = ClassMain.posIntConst;
            tempRow = ClassMain.rowConst;

            BoxDrop.Text = ClassMain.timerInitConst.ToString();
            BoxCol.Text = ClassMain.colConst.ToString();
            BoxRow.Text = ClassMain.rowConst.ToString();
            BoxPos.Text = ClassMain.posIntConst.ToString();
            BoxCombi.Text = ClassMain.combinaisonConst.ToString();
        }

        private void confirmation_Click(object sender, EventArgs e) //reverifie toutes les conditions, puis valide ou non
        {
            if (!verification(BoxCol.Text.ToString()) && !verification(BoxDrop.Text) && !verification(BoxRow.Text) && !verification(BoxPos.Text) && !verification(BoxCombi.Text))
            {
                if (Int32.Parse(BoxCombi.Text) < tempCol)
                {
                    if (Int32.Parse(BoxPos.Text) < tempCol)
                    {
                        if (Int32.Parse(BoxRow.Text) > 2)
                        {
                            if (Int32.Parse(BoxCol.Text) > 2)
                            {
                                if (Int32.Parse(BoxDrop.Text) >= 0)
                                {
                                    ClassMain.optionsOuvertes = false;
                                    ClassMain.col = tempCol;
                                    ClassMain.combinaison = tempCombi;
                                    ClassMain.timerInit = tempDrop;
                                    ClassMain.posInt = tempPos;
                                    ClassMain.row = tempRow;
                                    Options.ActiveForm.Close();
                                }
                                else
                                {
                                    MessageBox.Show("Il ne faut pas que le timer \n soit egal a 0");
                                    BoxCol.Text = ClassMain.col.ToString();
                                }
                            }
                            else
                            {
                                MessageBox.Show("nb de colonnes trop petite \n Il faut qu'elles soient superieur a 2");
                                BoxCol.Text = ClassMain.col.ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("nb de lignes trop petite \n Il faut qu'elles soient superieur a 2");
                            BoxRow.Text = ClassMain.row.ToString();
                        }

                    }
                    else
                    {
                        MessageBox.Show("Position trop grande \n Elle doit etre comprise entre 0 et " + (tempCol - 1).ToString());
                        BoxPos.Text = ClassMain.posInt.ToString();
                    }

                }
                else
                {
                    if (tempCol - 1 == 2)
                    {
                        tempCombi = 2;
                        BoxCombi.Text = tempCombi.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Combinaison trop grande \n Il faut qu'elle soit comprise entre 2 et " + (tempCol - 1).ToString());
                        BoxCombi.Text = ClassMain.combinaison.ToString();
                    }
                }

            }
        }

        private void Options_FormClosed(object sender, FormClosedEventArgs e) //quand on ferme la fenetre
        {
            ClassMain.optionsOuvertes = false;
        }
    }
}
